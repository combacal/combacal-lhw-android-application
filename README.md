# README #

***
**⚠️WARNING⚠️**

Builds on bitbucket currently don't run because the mapbox token is not setup on the remote. This was because the Project Management has not yet decided on a solution to use maps (See: https://combacal.atlassian.net/wiki/spaces/COMBACAL/pages/50364563/App+Architecture#Maps-API)
We did not want to use personal private tokens on the remote since this could result in costs.

***


### What is this repository for? ###

* This is the repository for the LHW Android App prototype
* V A0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Install Android Studio: https://developer.android.com/studio?hl=de
* Run the App: https://developer.android.com/training/basics/firstapp/running-app
* Use the developer guide to continue to develop: https://developer.android.com/guide
* Note: The SDK Version as well as Material have been updated since this project was finished. You might need to update!
* The Map part will not run since the credentials of Mapbox have not been set up. Follow these instructions to set it up: https://docs.mapbox.com/android/maps/guides/install/ . The public key belongs in a string resourece xml file called: developer-config.xml (It is in the .gitignore). Important note: Maps currently don't run offline. Follow the instructions here to set it up: https://docs.mapbox.com/android/maps/guides/offline/


### Contribution guidelines ###

* Use the bitbucket pull requests to implement new features
* Use Jira to create technical tasks from User Stories and create new Feature branches from there
* We use the gitflow branching model
* Architecture and coding style guides can be found on Confluence (https://combacal.atlassian.net/wiki/spaces/COMBACAL/pages/55673190/Frontend+-+LHW+Android+App)

### Who do I talk to? ###

The Android App up to this stage was implemented by Louis Bienz and Patrick Luchsinger with the help of Tlotlisso Mafantiri. Since Louis and Patrick are most likely not part of the project anymore, contact Tlotlisso in case of any questions.