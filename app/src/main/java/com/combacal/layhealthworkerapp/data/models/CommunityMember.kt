package com.combacal.layhealthworkerapp.data.models

import androidx.room.*
import java.util.*

@Entity(tableName = "communityMember",
        foreignKeys = [ForeignKey(entity = Household::class,
        parentColumns = ["householdId"],
        childColumns = ["householdId"],
        onDelete = ForeignKey.CASCADE)],
    indices = [Index(value = ["householdId"])])
data class CommunityMember(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "communityMemberId")
    val communityMemberId: Long = 0L,

    @ColumnInfo(name = "firstname")
    val firstname: String,

    @ColumnInfo(name = "lastname")
    val lastname: String,

    @ColumnInfo(name = "dateOfBirth")
    val dateOfBirth: Date,

    @ColumnInfo(name = "Sex")
    val sex: Sex,

    @ColumnInfo(name = "phoneNumber")
    val phoneNumber: String,

    @ColumnInfo(name = "householdId")
    val householdId: Long
)

enum class Sex {
    MALE,
    FEMALE,
    OTHER
}
