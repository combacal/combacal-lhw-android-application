package com.combacal.layhealthworkerapp.data.repositories

import androidx.lifecycle.LiveData
import com.combacal.layhealthworkerapp.data.datasources.daos.HouseholdDao
import com.combacal.layhealthworkerapp.data.models.Household
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.withContext


class HouseholdRepository(val datasource: HouseholdDao) {

    suspend fun insert(household: Household){
        withContext(Dispatchers.IO + NonCancellable) {
            datasource.insert(household)
        }
    }

    fun getAllHouseholds(): LiveData<List<Household>> {
        return datasource.getAllHouseholds()
    }

}