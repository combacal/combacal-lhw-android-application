package com.combacal.layhealthworkerapp.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.util.*


@Entity(tableName = "visit",
    foreignKeys = [androidx.room.ForeignKey(entity = CommunityMember::class,
    parentColumns = ["communityMemberId"],
    childColumns = ["communityMemberId"],
    onDelete = androidx.room.ForeignKey.CASCADE)],
    indices = [Index(value = ["communityMemberId"])])
data class Visit(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "visitId")
    val visitId: Long = 0L,

    @ColumnInfo(name = "dueDate")
    val dueDate: Date,

    @ColumnInfo(name = "visitStatus")
    val visitStatus: VisitStatus,

    @ColumnInfo(name = "communityMemberId")
    val communityMemberId: Long
)


enum class VisitStatus {
    OPEN, POSTPONED_ONCE, POSTPONED_TWICE, CRITICAL, CLOSED
}
