package com.combacal.layhealthworkerapp.data.datasources.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.combacal.layhealthworkerapp.data.models.Visit
import com.combacal.layhealthworkerapp.data.models.VisitWithTasks

@Dao
interface VisitDao : IBaseDao<Visit> {

    @Insert
    fun insertAndGetId(visit: Visit): Long

    @Transaction
    @Query("SELECT * from visit WHERE visitId = :id")
    fun getVisit(id: Long): VisitWithTasks

    @Transaction
    @Query("SELECT * from visit WHERE visitId = :id")
    fun getVisitAsLiveData(id: Long): LiveData<VisitWithTasks>

    @Query("SELECT * from visit")
    fun getAllVisitsAsLiveData(): LiveData<List<Visit>>
}