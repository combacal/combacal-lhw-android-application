package com.combacal.layhealthworkerapp.data.repositories

import com.combacal.layhealthworkerapp.data.datasources.daos.RecordDao
import com.combacal.layhealthworkerapp.data.models.Record
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.withContext

class RecordRepository(val datasource: RecordDao) {


    suspend fun insert(record: Record){
        withContext(Dispatchers.IO + NonCancellable) {
            datasource.insert(record)
        }
    }

}