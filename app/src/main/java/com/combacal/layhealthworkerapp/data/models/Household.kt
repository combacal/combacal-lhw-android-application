package com.combacal.layhealthworkerapp.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey



@Entity(tableName = "households")
data class Household(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "householdId")
    val householdId: Long = 0L,

    @ColumnInfo(name = "name")
    val name: String,

    @ColumnInfo(name = "latitude")
    val latitude: Double,

    @ColumnInfo(name = "longitude")
    val longitude: Double,

    @ColumnInfo(name = "head")
    val head: String,

    @ColumnInfo(name = "phone")
    val phone: String,

    @ColumnInfo(name = "locationDescription")
    val locationDescription: String)


