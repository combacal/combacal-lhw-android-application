package com.combacal.layhealthworkerapp.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "task",
    foreignKeys = [
        androidx.room.ForeignKey(entity = Visit::class,
            parentColumns = ["visitId"],
            childColumns = ["visitId"],
            onDelete = androidx.room.ForeignKey.CASCADE)
    ],
    indices = [Index(value = ["visitId"])])
data class Task(
    @PrimaryKey(autoGenerate = true)
    val taskId: Long = 0L,
    val medicalAlgorithmId: Long,
    val visitId: Long
)

