package com.combacal.layhealthworkerapp.data.models

import androidx.room.Embedded
import androidx.room.Relation

data class HouseholdWithCommunityMembers(
    @Embedded val household: Household,
    @Relation(
        parentColumn = "householdId",
        entityColumn = "householdId"
    )
    val communityMembers: List<CommunityMember>
)
