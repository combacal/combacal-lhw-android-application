package com.combacal.layhealthworkerapp.data.datasources.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.combacal.layhealthworkerapp.data.models.CommunityMember

@Dao
interface CommunityMemberDao : IBaseDao<CommunityMember> {

    @Query("SELECT * from communityMember WHERE communityMemberId = :id")
    fun get(id: Long): CommunityMember

    @Query("SELECT * from communityMember WHERE communityMemberId = :id")
    fun getAsLiveData(id: Long): LiveData<CommunityMember>

    @Query("Select * from communityMember WHERE householdId = :householdId")
    fun getByHouseholdId(householdId: Long): LiveData<List<CommunityMember>>

    @Query("SELECT * from communityMember WHERE lastname = :lastname")
    fun getByLastName(lastname: String): List<CommunityMember>

    @Query("SELECT * from communityMember ORDER BY communityMemberId DESC")
    fun getAllCommunityMembers(): List<CommunityMember>

    @Query("DELETE from communityMember")
    fun clear()
}