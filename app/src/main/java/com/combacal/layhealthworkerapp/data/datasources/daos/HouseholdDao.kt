package com.combacal.layhealthworkerapp.data.datasources.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.combacal.layhealthworkerapp.data.models.Household
import com.combacal.layhealthworkerapp.data.models.HouseholdWithCommunityMembers

@Dao
interface HouseholdDao : IBaseDao<Household> {

    @Transaction
    @Query("SELECT * from households WHERE householdId = :id")
    fun get(id: Long): LiveData<HouseholdWithCommunityMembers>

    @Query("SELECT * from households WHERE name = :name")
    fun getByName(name: String): Household

    @Query("SELECT * from households ORDER BY householdId DESC")
    fun getAllHouseholds(): LiveData<List<Household>>

    @Transaction
    @Query("SELECT * FROM households")
    fun getAllHouseholdsWithCommunityMembers(): List<HouseholdWithCommunityMembers>

    @Query("DELETE from households")
    fun clear()

}