package com.combacal.layhealthworkerapp.data.repositories

import androidx.lifecycle.LiveData
import com.combacal.layhealthworkerapp.data.datasources.daos.VisitDao
import com.combacal.layhealthworkerapp.data.models.Visit
import com.combacal.layhealthworkerapp.data.models.VisitWithTasks
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.withContext

class VisitRepository(val datasource: VisitDao) {

    suspend fun insertAndGetId(visit: Visit): Long {
        val visitId: Long
        withContext(Dispatchers.IO + NonCancellable) {
            visitId = datasource.insertAndGetId(visit)
        }
        return visitId
    }

    fun getAllVisitsAsLiveData(): LiveData<List<Visit>> {
        return datasource.getAllVisitsAsLiveData()
    }

    fun getVisitAsLiveData(id: Long): LiveData<VisitWithTasks> {
        return datasource.getVisitAsLiveData(id)
    }
}