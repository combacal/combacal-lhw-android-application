package com.combacal.layhealthworkerapp.data.models

import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import java.util.*



@Entity(tableName = "record",
    foreignKeys = [
        androidx.room.ForeignKey(entity = CommunityMember::class,
            parentColumns = ["communityMemberId"],
            childColumns = ["communityMemberId"],
            onDelete = androidx.room.ForeignKey.CASCADE),
        androidx.room.ForeignKey(entity = ActionItem::class,
            parentColumns = ["actionItemId"],
            childColumns = ["actionItemId"],
            onDelete = androidx.room.ForeignKey.NO_ACTION)
    ],
    indices = [Index(value = ["communityMemberId"]), Index(value = ["actionItemId"])])
data class Record(
    @PrimaryKey(autoGenerate = true)
    val recordId: Long = 0L,
    val observation: String,
    val actionItemId: Long,
    val communityMemberId: Long,
    val dateRecorded: Date
    )
