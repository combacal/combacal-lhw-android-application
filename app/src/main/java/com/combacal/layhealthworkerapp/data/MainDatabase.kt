package com.combacal.layhealthworkerapp.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.combacal.layhealthworkerapp.data.datasources.daos.*
import com.combacal.layhealthworkerapp.data.models.*
import com.combacal.layhealthworkerapp.data.models.Record
import com.combacal.layhealthworkerapp.data.datasources.daos.CommunityMemberDao
import com.combacal.layhealthworkerapp.data.datasources.daos.HouseholdDao
import com.combacal.layhealthworkerapp.data.datasources.daos.UserDao
import com.combacal.layhealthworkerapp.data.datasources.daos.VisitDao
import com.combacal.layhealthworkerapp.data.models.CommunityMember
import com.combacal.layhealthworkerapp.data.models.Household
import com.combacal.layhealthworkerapp.data.models.User
import com.combacal.layhealthworkerapp.data.models.Visit


// TODO: goal is to set exportSchema = true, figure out its behavior before setting to true

@Database(entities = [ActionItem::class, CommunityMember::class, Household::class, Record::class, Task::class, Visit::class, User::class],
    version = 17,
    exportSchema = false)
@TypeConverters(Converters::class)
abstract class MainDatabase : RoomDatabase() {

    abstract val householdDao: HouseholdDao
    abstract val communityMemberDao: CommunityMemberDao
    abstract val visitDao: VisitDao
    abstract val taskDao: TaskDao
    abstract val actionItemDao: ActionItemDao
    abstract val recordDao: RecordDao
    abstract val userDao: UserDao

    companion object {

        @Volatile
        private var INSTANCE: MainDatabase? = null

        fun getInstance(context: Context): MainDatabase {
            synchronized(this) {
                var instance = INSTANCE

                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        MainDatabase::class.java,
                        "households_history_database"
                    )
                        // normally here comes the database migration code
                        // e.g. how to adapt the existing database if the schema changes
                        // so far neglected, this wipes and rebuilds the database (data gets lost)
                        // starting point: https://developer.android.com/training/data-storage/room/migrating-db-versions
                        //                 https://medium.com/androiddevelopers/understanding-migrations-with-room-f01e04b07929
                        .fallbackToDestructiveMigration()
                        .build()
                    INSTANCE = instance
                }

                return instance
            }
        }
    }

}