package com.combacal.layhealthworkerapp.data.datasources.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Update
import com.combacal.layhealthworkerapp.data.models.Task

@Dao
interface TaskDao : IBaseDao<Task>