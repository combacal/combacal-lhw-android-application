package com.combacal.layhealthworkerapp.data.models

import androidx.room.Embedded
import androidx.room.Relation

data class VisitWithTasks (
    @Embedded
    val visit: Visit,
    @Relation(
        parentColumn = "visitId",
        entityColumn = "visitId"
    )
    val tasks: List<Task>
)