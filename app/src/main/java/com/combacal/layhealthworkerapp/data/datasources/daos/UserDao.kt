package com.combacal.layhealthworkerapp.data.datasources.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.combacal.layhealthworkerapp.data.models.User

@Dao
interface UserDao {

    @Insert
    fun insert(user: User)

    @Update
    fun update(user: User)

    @Query("SELECT * FROM users WHERE username LIKE :userName")
    fun getByUsername(userName: String): User?

    @Transaction
    @Query("SELECT * from users WHERE userId = :id")
    fun get(id: Long): LiveData<User>

    @Query("SELECT * from users ORDER BY userId DESC")
    fun getAllUsers(): LiveData<List<User>>

    @Query("DELETE from users")
    fun clear()
}
