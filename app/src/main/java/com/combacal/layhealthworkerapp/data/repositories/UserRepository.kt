package com.combacal.layhealthworkerapp.data.repositories

import androidx.lifecycle.LiveData
import com.combacal.layhealthworkerapp.data.datasources.daos.UserDao
import com.combacal.layhealthworkerapp.data.models.User
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.withContext

class UserRepository(private val datasource: UserDao) {

    val users = datasource.getAllUsers()

    suspend fun insert(user: User) {
        withContext(Dispatchers.IO + NonCancellable) {
            datasource.insert(user)
        }
    }

    suspend fun getByUserName(userName: String): User? {
        return withContext(Dispatchers.IO + NonCancellable) {
            datasource.getByUsername(userName)
        }
    }
}
