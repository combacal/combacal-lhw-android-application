package com.combacal.layhealthworkerapp.data.datasources.daos

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Update


interface IBaseDao<T> {

    // Insert a single object
    @Insert
    fun insert(obj: T)

    // Insert an array of an object
    @Insert
    fun insert(vararg obj: T)

    @Update
    fun update(obj: T)

    @Delete
    fun delete(obj: T)
}