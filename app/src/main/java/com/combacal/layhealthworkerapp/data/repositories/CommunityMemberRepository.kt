package com.combacal.layhealthworkerapp.data.repositories

import androidx.lifecycle.LiveData
import com.combacal.layhealthworkerapp.data.datasources.daos.CommunityMemberDao
import com.combacal.layhealthworkerapp.data.models.CommunityMember
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.withContext

class CommunityMemberRepository(val datasource: CommunityMemberDao) {


    suspend fun insertCommunityMember(communityMember: CommunityMember) =
        withContext(Dispatchers.IO + NonCancellable) {
            datasource.insert(communityMember)
        }

    fun getAsLiveData(id: Long): LiveData<CommunityMember> {
        return datasource.getAsLiveData(id)
    }

}