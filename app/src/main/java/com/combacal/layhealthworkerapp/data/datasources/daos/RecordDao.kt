package com.combacal.layhealthworkerapp.data.datasources.daos

import androidx.room.Dao
import com.combacal.layhealthworkerapp.data.models.Record

@Dao
interface RecordDao : IBaseDao<Record>