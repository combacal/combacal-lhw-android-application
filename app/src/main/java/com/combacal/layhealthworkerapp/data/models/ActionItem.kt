package com.combacal.layhealthworkerapp.data.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "actionItem")
data class ActionItem(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "actionItemId")
    val actionItemId: Long = 0L,

    @ColumnInfo(name = "question")
    val question: String,

    @ColumnInfo(name = "recordType")
    val recordType: RecordType
)

enum class RecordType {
    BOOLEAN, LIKERT, NUMBER, FREE_TEXT
}