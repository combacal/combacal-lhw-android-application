package com.combacal.layhealthworkerapp.data.repositories

import com.combacal.layhealthworkerapp.data.datasources.daos.TaskDao
import com.combacal.layhealthworkerapp.data.models.Task
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.NonCancellable
import kotlinx.coroutines.withContext

class TaskRepository(val datasource: TaskDao) {

    suspend fun insert(task: Task) {
        withContext(Dispatchers.IO + NonCancellable) {
            datasource.insert(task)
        }
    }
}