package com.combacal.layhealthworkerapp.data.models

import androidx.room.Embedded
import androidx.room.Relation

data class CommunityMemberWithVisits(
    @Embedded val communityMember: CommunityMember,
    @Relation(
        parentColumn = "communityMemberId",
        entityColumn = "communityMemberId"
    )
    val communityMembers: List<Visit>

)
