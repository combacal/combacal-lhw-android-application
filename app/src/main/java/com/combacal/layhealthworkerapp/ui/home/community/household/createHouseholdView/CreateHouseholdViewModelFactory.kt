package com.combacal.layhealthworkerapp.ui.home.community.household.createHouseholdView

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.combacal.layhealthworkerapp.data.repositories.HouseholdRepository

class CreateHouseholdViewModelFactory(
    private val repository: HouseholdRepository
) : ViewModelProvider.Factory {

    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CreateHouseholdViewModel::class.java)) {
            return CreateHouseholdViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}