package com.combacal.layhealthworkerapp.ui.home

import android.os.Bundle
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.combacal.layhealthworkerapp.R
import com.combacal.layhealthworkerapp.databinding.ActivityMainBinding
import com.combacal.layhealthworkerapp.ui.home.agenda.AgendaFragmentDirections
import com.combacal.layhealthworkerapp.ui.home.community.CommunityFragmentDirections
import com.combacal.layhealthworkerapp.ui.home.community.household.householdView.HouseholdFragmentArgs
import com.combacal.layhealthworkerapp.ui.home.community.household.householdView.HouseholdFragmentDirections
import com.mapbox.mapboxsdk.Mapbox


interface SupportActionBarListener {

    fun setSupportActionBarTitle(title: String)

}

class MainActivity : AppCompatActivity(), SupportActionBarListener {

    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Hide fab when creating the activity to make sure it's hidden if not needed!
        // Alternatively fab can be hidden with an xml attribute, but then its not visible in the layout editor
        binding.floatingActionButtonAdd.hide()
        findAndSetNavController()

        val navView: BottomNavigationView = binding.navView


        setupBottomNavigationController(navView)

        // Setup Mapbox
        Mapbox.getInstance(this, getString(R.string.mapbox_access_token))

    }

    private fun findAndSetNavController() {
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment_activity_main) as NavHostFragment
        navController = navHostFragment.navController
        navController.addOnDestinationChangedListener { controller, destination, arguments ->
            val fab = binding.floatingActionButtonAdd
            fab.hide()
            when(destination.id) {
                R.id.navigation_community -> setupCreateHouseholdFab(controller)
                R.id.householdFragment -> setupCreateCommunityMemberFab(controller, arguments)
                R.id.navigation_agenda -> setupCreateVisitFab(controller)
            }
        }
    }


    private fun setupCreateHouseholdFab(controller: NavController) {
        val fab = binding.floatingActionButtonAdd
        fab.contentDescription = R.string.fab_add_household.toString()
        fab.setOnClickListener {
            controller.navigate(CommunityFragmentDirections.actionNavigationCommunityToFragmentCreateHousehold())
        }
        fab.show()
    }

    private fun setupCreateCommunityMemberFab(controller: NavController, arguments: Bundle?) {
        val fab = binding.floatingActionButtonAdd
        fab.contentDescription = R.string.fab_add_community_member.toString()
        fab.setOnClickListener {
            val args = HouseholdFragmentArgs.fromBundle(arguments!!)
            controller.navigate(
                HouseholdFragmentDirections
                .actionHouseholdFragmentToCreateCommunityMemberFragment(args.householdId))
        }
        fab.show()
    }

    private fun setupCreateVisitFab(controller: NavController) {
        val fab = binding.floatingActionButtonAdd
        fab.contentDescription = R.string.fab_add_visit.toString()
        fab.setOnClickListener {
            controller.navigate(AgendaFragmentDirections.actionNavigationAgendaToCreateVisitView())
        }
        fab.show()
    }





    private fun setupBottomNavigationController(navView: BottomNavigationView) {
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_community,
                R.id.navigation_agenda,
                R.id.navigation_training
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp()
    }

    override fun setSupportActionBarTitle(title: String) {
        supportActionBar?.title = title
    }
}