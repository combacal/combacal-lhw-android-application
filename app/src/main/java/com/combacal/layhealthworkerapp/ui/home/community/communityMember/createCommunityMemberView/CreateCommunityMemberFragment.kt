package com.combacal.layhealthworkerapp.ui.home.community.communityMember.createCommunityMemberView

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.telephony.PhoneNumberFormattingTextWatcher
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.combacal.layhealthworkerapp.R
import com.combacal.layhealthworkerapp.data.MainDatabase
import com.combacal.layhealthworkerapp.data.repositories.CommunityMemberRepository
import com.combacal.layhealthworkerapp.databinding.FragmentCreateCommunityMemberBinding
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointBackward
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.snackbar.Snackbar
import java.text.SimpleDateFormat
import java.util.*

class CreateCommunityMemberFragment : Fragment() {

    private lateinit var viewModel: CreateCommunityMemberViewModel

    private var _binding: FragmentCreateCommunityMemberBinding? = null
    private val binding get() = _binding!!


    // -- Lifecycle methods
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        _binding = FragmentCreateCommunityMemberBinding.inflate(inflater, container, false)

        setupAndBindViewModelWithDatasource()
        setupDateOfBirthPicker()
        setupTextWatcherForPhoneNumberField()
        setupTextWatcherForFormFields()
        observeNavigateToHouseholdView()
        observeShowSnackbarCommunityMemberAddedEvent()

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    // -- Setup --

    private fun setupAndBindViewModelWithDatasource() {
        val args = CreateCommunityMemberFragmentArgs.fromBundle(requireArguments())
        val application = requireNotNull(this.activity).application
        val datasource = MainDatabase.getInstance(application).communityMemberDao
        val repository = CommunityMemberRepository(datasource)
        val viewModelFactory = CreateCommunityMemberViewModelFactory(repository, application, args.householdId)

        viewModel =
            ViewModelProvider(
                this, viewModelFactory
            ).get(CreateCommunityMemberViewModel::class.java)

        _binding!!.createCommunityMemberViewModel = viewModel


        val items = listOf("Male", "Female", "Other")
        val adapter = ArrayAdapter(requireContext(), R.layout.list_item_dropdown_menu, items)
        (binding.editTextSex as? AutoCompleteTextView)?.setAdapter(adapter)

        _binding!!.lifecycleOwner = viewLifecycleOwner
    }

    // -- Field Input Logic --

    private fun setupTextWatcherForPhoneNumberField() {
        binding.editTextPhoneNumberCommunityMember.addTextChangedListener(PhoneNumberFormattingTextWatcher())
    }

    private fun setupDateOfBirthPicker() {
        val dateOfBirthTextView = binding.editTextDateOfBirth as TextView

        dateOfBirthTextView.setOnClickListener {
            val calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"))

            calendar[Calendar.YEAR] = 2005
            val year2005 = calendar.timeInMillis

            // Calendar constraints (Date has to lie in the past)
            val constraintsBuilder =
                CalendarConstraints.Builder()
                    .setValidator(DateValidatorPointBackward.now())
                    .setOpenAt(year2005)


            val datePicker =
                MaterialDatePicker.Builder.datePicker()
                    .setTitleText("Select Date of Birth")
                    .setCalendarConstraints(constraintsBuilder.build())
                    .build()

            datePicker.addOnPositiveButtonClickListener {
                val date = Date(it)
                val format = SimpleDateFormat("dd/MM/yyyy")
                val dateString = format.format(date)
                dateOfBirthTextView.text = dateString
                viewModel.dateOfBirthFormDataChanged(date)
            }

            datePicker.show(requireActivity().supportFragmentManager, "datePicker")
        }
    }

    private fun setupTextWatcherForFormFields() {
        val afterTextChangedListener = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // ignore
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // ignore
            }

            override fun afterTextChanged(s: Editable?) {
                viewModel.createCommunityMemberFormDataChanged(
                    binding.editTextFirstName.text.toString(),
                    binding.editTextLastName.text.toString(),
                    binding.editTextDateOfBirth.text.toString(),
                    binding.editTextSex.text.toString(),
                    binding.editTextPhoneNumberCommunityMember.text.toString()
                )
            }
        }

        binding.editTextFirstName.addTextChangedListener(afterTextChangedListener)
        binding.editTextLastName.addTextChangedListener(afterTextChangedListener)
        binding.editTextDateOfBirth.addTextChangedListener(afterTextChangedListener)
        binding.editTextSex.addTextChangedListener(afterTextChangedListener)
        binding.editTextPhoneNumberCommunityMember.addTextChangedListener(afterTextChangedListener)
    }







    // -- Navigation Observers --

    private fun observeNavigateToHouseholdView() {
        viewModel.navigateToHouseholdView.observe(viewLifecycleOwner, {
            if (it == true) {
                findNavController().navigateUp()
                viewModel.doneNavigatingToHouseholdView()
            }
        })
    }

    private fun observeShowSnackbarCommunityMemberAddedEvent() {
        viewModel.showSnackbarCommunityMemberAddedEvent.observe(viewLifecycleOwner, {
            if (it == true) {
                Snackbar.make(
                    requireActivity().findViewById(R.id.snack_bar_host),
                    getString(R.string.community_member_added),
                    Snackbar.LENGTH_SHORT
                ).show()
                viewModel.doneShowingSnackbarHouseholdAddedEvent()
            }
        })
    }
}