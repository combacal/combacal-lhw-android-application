package com.combacal.layhealthworkerapp.ui.home.agenda.createVisitView

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.combacal.layhealthworkerapp.data.repositories.RecordRepository
import com.combacal.layhealthworkerapp.data.repositories.VisitRepository

class CreateVisitViewModelFactory(
    private val repository: VisitRepository,
    private val recordRepository: RecordRepository
) : ViewModelProvider.Factory {

    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CreateVisitViewModel::class.java)) {
            return CreateVisitViewModel(repository, recordRepository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}