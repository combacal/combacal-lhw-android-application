package com.combacal.layhealthworkerapp.ui.home.community.communityMember.createCommunityMemberView

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.combacal.layhealthworkerapp.data.models.CommunityMember
import com.combacal.layhealthworkerapp.data.models.Sex
import com.combacal.layhealthworkerapp.data.repositories.CommunityMemberRepository
import kotlinx.coroutines.launch
import java.util.*

class CreateCommunityMemberViewModel (
    val repository: CommunityMemberRepository,
    application: Application,
    val householdId: Long
) : AndroidViewModel(application) {

    // -- Navigation Live Data --
    private val _navigateToHouseholdView = MutableLiveData<Boolean>(false)
    val navigateToHouseholdView: LiveData<Boolean>
        get() = _navigateToHouseholdView

    // -- Interactive Elements Live Data --
    private val _showSnackbarCommunityMemberAddedEvent = MutableLiveData<Boolean>(false)
    val showSnackbarCommunityMemberAddedEvent: LiveData<Boolean>
        get() = _showSnackbarCommunityMemberAddedEvent

    // -- Input Fields --
    private val  _isFormComplete = MutableLiveData<Boolean>()
    val isFormComplete: LiveData<Boolean>
        get() = _isFormComplete

    private lateinit var firstName : String
    private lateinit var lastName : String
    private lateinit var dateOfBirth : Date
    private lateinit var sex : Sex
    private lateinit var phoneNumber : String

    fun dateOfBirthFormDataChanged(dateOfBirth: Date) {
        this.dateOfBirth = dateOfBirth
    }

    fun createCommunityMemberFormDataChanged(firstName: String,
                                             lastName: String,
                                             dateOfBirth: String,
                                             sex: String,
                                             phoneNumber: String) {

        val isAnyFieldEmpty = checkIfAnyFieldIsEmpty(
            listOf(
                firstName,
                lastName,
                dateOfBirth,
                sex
            )
        )

        if (isAnyFieldEmpty) { _isFormComplete.value = false; return }

        this.firstName = firstName
        this.lastName = lastName
        when(sex) {
            "Male" -> this.sex = Sex.MALE
            "Female" -> this.sex = Sex.FEMALE
            "Other" -> this.sex = Sex.OTHER
        }
        this.phoneNumber = phoneNumber
        _isFormComplete.value = true

    }

    private fun checkIfAnyFieldIsEmpty(createHouseholdFormData: List<String>): Boolean {
        return createHouseholdFormData.contains(String())
    }


    // -- Add button --
    fun onAddCommunityMemberClicked() {
        // https://developer.android.com/kotlin/coroutines
        viewModelScope.launch {
            val communityMember = CommunityMember(
                firstname = firstName,
                lastname = lastName,
                dateOfBirth = dateOfBirth,
                sex = sex,
                phoneNumber = phoneNumber,
                householdId = householdId)
            repository.insertCommunityMember(communityMember)
        }
        _navigateToHouseholdView.value = true
        _showSnackbarCommunityMemberAddedEvent.value = true
    }

    fun doneNavigatingToHouseholdView() {
        _navigateToHouseholdView.value = false
    }

    fun doneShowingSnackbarHouseholdAddedEvent() {
        _showSnackbarCommunityMemberAddedEvent.value = false
    }


}

