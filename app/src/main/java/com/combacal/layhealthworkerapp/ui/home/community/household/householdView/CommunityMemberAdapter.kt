package com.combacal.layhealthworkerapp.ui.home.community.household.householdView

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.combacal.layhealthworkerapp.data.models.CommunityMember
import com.combacal.layhealthworkerapp.databinding.ListItemCommunityMemberBinding

class CommunityMemberAdapter(val clickListener: CommunityMemberListener) : ListAdapter<CommunityMember, CommunityMemberAdapter.CommunityMemberViewHolder>(
    CommunityMemberDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommunityMemberViewHolder {
        return CommunityMemberViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: CommunityMemberViewHolder, position: Int) {
        holder.bind(getItem(position)!!, clickListener) // pass clickListener down to viewHolder
    }


    class CommunityMemberViewHolder private constructor(val binding: ListItemCommunityMemberBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(
            item: CommunityMember,
            clickListener: CommunityMemberListener
        ) {
            binding.communityMember = item
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): CommunityMemberViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemCommunityMemberBinding.inflate(layoutInflater, parent, false)

                return CommunityMemberViewHolder(binding)
            }
        }
    }

}

class CommunityMemberDiffCallback: DiffUtil.ItemCallback<CommunityMember>() {
    override fun areItemsTheSame(oldItem: CommunityMember, newItem: CommunityMember): Boolean {
        return oldItem.communityMemberId == newItem.communityMemberId
    }

    override fun areContentsTheSame(oldItem: CommunityMember, newItem: CommunityMember): Boolean {
        return oldItem == newItem
    }
}

class CommunityMemberListener(val clickListener: (communitMemberId: Long) -> Unit) {
    fun onClick(communityMember: CommunityMember) = clickListener(communityMember.communityMemberId)
}