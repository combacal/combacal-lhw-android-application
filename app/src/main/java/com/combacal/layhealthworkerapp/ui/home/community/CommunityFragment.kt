package com.combacal.layhealthworkerapp.ui.home.community

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.*
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import com.combacal.layhealthworkerapp.R
import com.combacal.layhealthworkerapp.data.MainDatabase
import com.combacal.layhealthworkerapp.data.repositories.HouseholdRepository
import com.combacal.layhealthworkerapp.databinding.FragmentCommunityBinding

class CommunityFragment : Fragment() {


    private lateinit var viewModel: CommunityViewModel

    private var _binding: FragmentCommunityBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentCommunityBinding.inflate(inflater, container, false)

        setupAndBindViewModelWithRepository()

        addOnToggleButtonCheckedListener()

        observeCommunityViewType()
        observeNavigateToHousehold()

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    // helper functions
    private fun findNestedNavController(): NavController? {
        val nestedNavGraph =
            childFragmentManager.findFragmentById(R.id.nav_host_fragment_fragment_community) as? NavHostFragment
        return nestedNavGraph?.navController
    }


    // functions used in OnCreateView()

    private fun observeNavigateToHousehold() {
        viewModel.navigateToHousehold.observe(viewLifecycleOwner, { householdId ->
            householdId?.let {
                findNavController().navigate(
                    CommunityFragmentDirections
                        .actionGlobalHouseholdFragment(householdId)
                )
                viewModel.doneNavigatingToHousehold()
            }
        })
    }

    private fun observeCommunityViewType() {
        viewModel.communityViewType.observe(viewLifecycleOwner, { viewType ->
            when (viewType) {
                CommunityViewType.MAPVIEW -> findNestedNavController()?.navigate(R.id.communityMapViewFragment)
                CommunityViewType.LISTVIEW -> findNestedNavController()?.navigate(R.id.communityListViewFragment)
                else -> findNestedNavController()?.navigate(R.id.communityListViewFragment)
            }
        })
    }

    private fun setupAndBindViewModelWithRepository() {
        val application = requireNotNull(this.activity).application
        val datasource = MainDatabase.getInstance(application).householdDao
        val repository = HouseholdRepository(datasource)
        val viewModelFactory = CommunityViewModelFactory(repository, application)

        viewModel =
            ViewModelProvider(
                requireActivity(), viewModelFactory
            ).get(CommunityViewModel::class.java)

        _binding!!.communityViewModel = viewModel

        // Set itself as the lifecycleOwner of the binding
        _binding!!.lifecycleOwner = viewLifecycleOwner
    }

    private fun addOnToggleButtonCheckedListener() {
        binding.toggleGroupCommunityView.addOnButtonCheckedListener { _, checkedId, isChecked ->

            if (!isChecked) return@addOnButtonCheckedListener

            when (checkedId) {
                binding.toggleButtonMapView.id -> viewModel.onMapButtonSelected()
                binding.toggleButtonListView.id -> viewModel.onListButtonSelected()
            }
        }
    }


}