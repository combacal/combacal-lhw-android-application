package com.combacal.layhealthworkerapp.ui.home.community

import java.time.LocalDate
import java.time.Period
import java.time.ZoneId
import java.util.*

fun convertDateOfBirthToAge(dateOfBirth: Date): String {

    val today = LocalDate.now()
    val birthday = dateOfBirth.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()

    val period = Period.between(birthday, today)

    return period.years.toString()

}