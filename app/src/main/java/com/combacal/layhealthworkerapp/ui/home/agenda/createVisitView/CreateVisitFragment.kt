package com.combacal.layhealthworkerapp.ui.home.agenda.createVisitView

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.combacal.layhealthworkerapp.R
import com.combacal.layhealthworkerapp.data.MainDatabase
import com.combacal.layhealthworkerapp.data.repositories.RecordRepository
import com.combacal.layhealthworkerapp.data.repositories.VisitRepository
import com.combacal.layhealthworkerapp.databinding.FragmentCreateVisitViewBinding
import com.google.android.material.snackbar.Snackbar

class CreateVisitFragment : Fragment() {

    private var _binding: FragmentCreateVisitViewBinding? = null
    private val binding get() = _binding!!

    private lateinit var viewModel: CreateVisitViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentCreateVisitViewBinding.inflate(inflater, container, false)

        setupAndBindViewModelWithRepository()
        observeNavigateBack()
        observeSnackbarVisitAddedEvent()


        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupAndBindViewModelWithRepository() {
        val application = requireNotNull(this.activity).application
        val datasource = MainDatabase.getInstance(application).visitDao
        val recordDao = MainDatabase.getInstance(application).recordDao

        val recordRepository = RecordRepository(recordDao)
        val repository = VisitRepository(datasource)
        val viewModelFactory = CreateVisitViewModelFactory(repository, recordRepository)

        viewModel =
            ViewModelProvider(
                this, viewModelFactory
            ).get(CreateVisitViewModel::class.java)

        _binding!!.createVisitViewModel = viewModel
        _binding!!.lifecycleOwner = viewLifecycleOwner
    }

    private fun observeNavigateBack() {
        viewModel.navigateBack.observe(viewLifecycleOwner, {
            if(it == true) {
                findNavController().navigateUp()
                viewModel.doneNavigatingBack()
            }
        })
    }

    private fun observeSnackbarVisitAddedEvent() {
        viewModel.showSnackbarVisitAddedEvent.observe(viewLifecycleOwner, {
            if(it == true) {
                Snackbar.make(
                    requireActivity().findViewById(R.id.snack_bar_host),
                    getString(R.string.visit_added),
                    Snackbar.LENGTH_SHORT
                ).show()
                viewModel.doneShowingSnackbarVisitAddedEvent()
            }
        })
    }

}