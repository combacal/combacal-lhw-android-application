package com.combacal.layhealthworkerapp.ui.home.community.communityMember.communityMemberView

import androidx.lifecycle.ViewModel
import com.combacal.layhealthworkerapp.data.repositories.CommunityMemberRepository

class CommunityMemberViewModel(
    repository: CommunityMemberRepository,
    communityMemberId: Long,
) : ViewModel() {
    // Community Member Live Data
    var communityMember = repository.getAsLiveData(communityMemberId)

}