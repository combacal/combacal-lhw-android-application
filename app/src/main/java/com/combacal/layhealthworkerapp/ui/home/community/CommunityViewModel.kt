package com.combacal.layhealthworkerapp.ui.home.community

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.combacal.layhealthworkerapp.data.repositories.HouseholdRepository

enum class CommunityViewType() {
    MAPVIEW, LISTVIEW
}


class CommunityViewModel(
    repository: HouseholdRepository,
    application: Application,
) : AndroidViewModel(application) {

    val households = repository.getAllHouseholds()


    private val _navigateToHousehold = MutableLiveData<Long?>()
    val navigateToHousehold: MutableLiveData<Long?>
        get() = _navigateToHousehold

    fun doneNavigatingToHousehold() {
        _navigateToHousehold.value = null
    }


    private var _communityViewType = MutableLiveData<CommunityViewType>()
    val communityViewType: LiveData<CommunityViewType>
        get() = _communityViewType


    init {
        _communityViewType.value = CommunityViewType.MAPVIEW

    }

    fun onMapButtonSelected() {
        _communityViewType.value = CommunityViewType.MAPVIEW
    }

    fun onListButtonSelected() {
        _communityViewType.value = CommunityViewType.LISTVIEW
    }


    fun onHouseholdClicked(householdId: Long) {
        _navigateToHousehold.value = householdId
    }
}