package com.combacal.layhealthworkerapp.ui.home.community.communityMapView

import android.annotation.SuppressLint
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.combacal.layhealthworkerapp.R
import com.combacal.layhealthworkerapp.data.MainDatabase
import com.combacal.layhealthworkerapp.data.repositories.HouseholdRepository
import com.combacal.layhealthworkerapp.databinding.FragmentCommunityMapViewBinding
import com.combacal.layhealthworkerapp.ui.home.community.CommunityViewModel
import com.combacal.layhealthworkerapp.ui.home.community.CommunityViewModelFactory
import com.google.android.material.snackbar.Snackbar
import com.mapbox.android.core.permissions.PermissionsListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.LocationComponentOptions
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.plugins.annotation.OnSymbolClickListener
import com.mapbox.mapboxsdk.plugins.annotation.Symbol
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager


class CommunityMapViewFragment : Fragment(), OnMapReadyCallback, Style.OnStyleLoaded, PermissionsListener,
    OnSymbolClickListener {

    private lateinit var communityViewModel: CommunityViewModel

    private var _binding: FragmentCommunityMapViewBinding? = null
    private val binding get() = _binding!!

    private var permissionsManager: PermissionsManager = PermissionsManager(this)

    private lateinit var mapView: MapView
    private lateinit var mapboxMap: MapboxMap
    private lateinit var symbolManager: SymbolManager
    private lateinit var communityMapViewAdapter: CommunityMapViewAdapter

    // -- Lifecycle methods --
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        _binding = FragmentCommunityMapViewBinding.inflate(inflater, container, false)

        setupMapView(savedInstanceState)
        setupAndBindViewModelWithRepository()

        return binding.root
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
        mapView.onDestroy()
    }

    // -- Instantiation methods --
    private fun setupAndBindViewModelWithRepository() {
        val application = requireNotNull(this.activity).application
        val datasource = MainDatabase.getInstance(application).householdDao
        val repository = HouseholdRepository(datasource)
        val viewModelFactory = CommunityViewModelFactory(repository, application)

        communityViewModel =
            ViewModelProvider(
                requireActivity(), viewModelFactory).get(CommunityViewModel::class.java)

        _binding!!.communityViewModel = communityViewModel
        _binding!!.lifecycleOwner = viewLifecycleOwner
    }

    private fun setupMapView(savedInstanceState: Bundle?) {
        mapView = binding.mapViewCommunity
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)
    }


    // -- Mapview instantiation callbacks --
    override fun onMapReady(mapboxMap: MapboxMap) {
        this.mapboxMap = mapboxMap
        mapboxMap.setStyle(Style.SATELLITE_STREETS, this)
        mapboxMap.setMinZoomPreference(8.0)
        mapboxMap.setLatLngBoundsForCameraTarget(LatLngBounds.from(-28.5, 29.5, -30.7, 27.0))


    }

    override fun onStyleLoaded(style: Style) {

        // TODO: Think about the best way to import the necessary resources
        // At the moment this is still buggy
        val icon = BitmapFactory.decodeResource(resources, R.drawable.red_marker)
        style.addImage("home", icon)

        // Old version where image disappears after zooming in and out too often
//        val icon = getDrawable(requireContext(), R.drawable.ic_home_black_24dp)
//        if (icon != null) {
//            style.addImage("home", icon)
//        }

        // Alternative version where image disappears after zooming in and out too often
//        val icon = BitmapUtils.getBitmapFromDrawable(getDrawable(requireContext(), R.drawable.ic_home_black_24dp))
//        style.addImage("home", icon!!, true)

        symbolManager = SymbolManager(mapView, mapboxMap, style)
        symbolManager.iconAllowOverlap = true
        symbolManager.textAllowOverlap = true
        symbolManager.iconIgnorePlacement = true
        symbolManager.textIgnorePlacement = true
        symbolManager.addClickListener(this)


        communityMapViewAdapter = CommunityMapViewAdapter(symbolManager)
        observeHouseholds()

        // I kept this in for the moment since we probably need this for debugging
//        mapboxMap.addOnScaleListener(object : MapboxMap.OnScaleListener {
//            override fun onScaleBegin(detector: StandardScaleGestureDetector) {
//                return
//            }
//
//            override fun onScale(detector: StandardScaleGestureDetector) {
//                Log.i("MapView", symbolManager.annotations.toString())
//                Log.i("MapView", style.layers.toString())
//                Log.i("MapView", style.getLayer(symbolManager.layerId)?.visibility.toString())
//            }
//
//            override fun onScaleEnd(detector: StandardScaleGestureDetector) {
//                return
//            }
//        })

        enableLocationTrackingAndDisplay(style)
    }

    // -- Enable the location tracking of the LHW --

    @SuppressLint("MissingPermission")
    private fun enableLocationTrackingAndDisplay(loadedMapStyle: Style) {

        // Check if permissions are enabled and if not request
        if (!PermissionsManager.areLocationPermissionsGranted(requireContext())) {
            permissionsManager = PermissionsManager(this)
            permissionsManager.requestLocationPermissions(this.activity)
            return
        }

        // Create and customize the LocationComponent's options
        val customLocationComponentOptions = LocationComponentOptions.builder(requireContext())
            .trackingGesturesManagement(true)
            .accuracyColor(ContextCompat.getColor(requireContext(), R.color.status_green))
            .build()

        val locationComponentActivationOptions = LocationComponentActivationOptions.builder(requireContext(), loadedMapStyle)
            .locationComponentOptions(customLocationComponentOptions)
            .build()

        // Get an instance of the LocationComponent and then adjust its settings
        mapboxMap.locationComponent.apply {

            // Activate the LocationComponent with options
            activateLocationComponent(locationComponentActivationOptions)

            // Enable to make the LocationComponent visible
            isLocationComponentEnabled = true

            // Set the LocationComponent's camera mode
            cameraMode = CameraMode.TRACKING

            // Set the LocationComponent's render mode
            renderMode = RenderMode.COMPASS
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onExplanationNeeded(permissionsToExplain: List<String>) {
        Snackbar.make(binding.root, R.string.user_location_permission_explanation, Snackbar.LENGTH_LONG).show()

    }

    override fun onPermissionResult(granted: Boolean) {
        if (granted) {
            enableLocationTrackingAndDisplay(mapboxMap.style!!)
        } else {
            Snackbar.make(binding.root, R.string.user_location_permission_not_granted, Snackbar.LENGTH_LONG).show()
        }
    }


    // -- Observers --
    private fun observeHouseholds() {
        communityViewModel.households.observe(viewLifecycleOwner, { households ->
            communityMapViewAdapter.data = households
        })
    }

    override fun onAnnotationClick(t: Symbol?): Boolean {
        t?.data?.let { communityViewModel.onHouseholdClicked(it.asLong) }
        return true
    }



}