package com.combacal.layhealthworkerapp.ui.home.agenda.visitView

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.combacal.layhealthworkerapp.data.repositories.VisitRepository

class VisitViewModelFactory(private val repository: VisitRepository, private val visitId: Long) :
    ViewModelProvider.Factory {

    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(VisitViewModel::class.java)) {
            return VisitViewModel(repository, visitId) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}