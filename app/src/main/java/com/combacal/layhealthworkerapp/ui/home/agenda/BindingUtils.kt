package com.combacal.layhealthworkerapp.ui.home.agenda

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.combacal.layhealthworkerapp.R
import com.combacal.layhealthworkerapp.data.models.Visit

@BindingAdapter("visitType")
fun TextView.setVisitType(item: Visit?){
    item?.let {
        text = "Update to tasktypes"
    }
}

@BindingAdapter("visitStatus")
fun ImageView.setVisitStatusImage(item: Visit?) {
    item?.let {
        setImageResource(R.drawable.ic_baseline_check_box_outline_blank_24)
    }
}