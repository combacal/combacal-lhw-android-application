package com.combacal.layhealthworkerapp.ui.home.community.household.createHouseholdView

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.combacal.layhealthworkerapp.R
import com.combacal.layhealthworkerapp.data.MainDatabase
import com.combacal.layhealthworkerapp.data.repositories.HouseholdRepository
import com.combacal.layhealthworkerapp.databinding.FragmentCreateHouseholdBinding
import com.google.android.material.snackbar.Snackbar

class CreateHouseholdFragment : Fragment() {

    private lateinit var viewModel: CreateHouseholdViewModel

    private var _binding: FragmentCreateHouseholdBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentCreateHouseholdBinding.inflate(inflater, container, false)

        setupAndBindViewModelWithRepository()

        observeNavigateToCommunityView()
        observeShowSnackbarHouseholdAddedEvent()

        setupTextWatcherForFormFields()

        return binding.root
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    private fun setupTextWatcherForFormFields() {
        val afterTextChangedListener = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // ignore
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // ignore
            }

            override fun afterTextChanged(s: Editable?) {
                viewModel.createHouseholdFormDataChanged(
                    binding.editTextHouseholdName.text.toString(),
                    binding.editTextFirstNameHeadOfHousehold.text.toString(),
                    binding.editTextPhoneNumberHeadOfHousehold.text.toString(),
                    binding.editTextHouseholdDescription.text.toString()

                )
            }
        }

        binding.editTextHouseholdName.addTextChangedListener(afterTextChangedListener)
        binding.editTextFirstNameHeadOfHousehold.addTextChangedListener(afterTextChangedListener)
        binding.editTextPhoneNumberHeadOfHousehold.addTextChangedListener(afterTextChangedListener)
        binding.editTextHouseholdDescription.addTextChangedListener(afterTextChangedListener)
    }

    private fun setupAndBindViewModelWithRepository() {
        val application = requireNotNull(this.activity).application
        val datasource = MainDatabase.getInstance(application).householdDao
        val repository = HouseholdRepository(datasource)
        val viewModelFactory = CreateHouseholdViewModelFactory(repository)

        viewModel =
            ViewModelProvider(
                this, viewModelFactory
            ).get(CreateHouseholdViewModel::class.java)

        _binding!!.createHouseholdViewModel = viewModel
        _binding!!.lifecycleOwner = viewLifecycleOwner
    }

    private fun observeNavigateToCommunityView() {
        viewModel.navigateToCommunityView.observe(viewLifecycleOwner, {
            if (it == true) {
                findNavController().navigateUp()
                viewModel.doneNavigatingToCommunityView()
            }
        })
    }

    private fun observeShowSnackbarHouseholdAddedEvent() {
        viewModel.showSnackbarHouseholdAddedEvent.observe(viewLifecycleOwner, {
            if (it == true) {
                Snackbar.make(
                    requireActivity().findViewById(R.id.snack_bar_host),
                    getString(R.string.household_added),
                    Snackbar.LENGTH_SHORT
                ).show()
                viewModel.doneShowingSnackbarHouseholdAddedEvent()
            }
        })
    }
}