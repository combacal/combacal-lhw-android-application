package com.combacal.layhealthworkerapp.ui.home.agenda.createVisitView

import android.database.sqlite.SQLiteConstraintException
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.combacal.layhealthworkerapp.data.models.*
import com.combacal.layhealthworkerapp.data.models.Record
import com.combacal.layhealthworkerapp.data.repositories.RecordRepository
import com.combacal.layhealthworkerapp.data.repositories.VisitRepository
import kotlinx.coroutines.launch
import java.util.*

class CreateVisitViewModel(
    val repository: VisitRepository,
    val recordRepository: RecordRepository
) : ViewModel() {

    private val _navigateBack = MutableLiveData<Boolean>()
    val navigateBack: LiveData<Boolean>
        get() = _navigateBack

    private val _showSnackbarVisitAddedEvent = MutableLiveData<Boolean>()
    val showSnackbarVisitAddedEvent: LiveData<Boolean>
        get() = _showSnackbarVisitAddedEvent


    fun onAddVisitClicked() {
        viewModelScope.launch {
            val visit =
                Visit(dueDate = Date(), visitStatus = VisitStatus.OPEN, communityMemberId = 1)
            try {
//                val visitId = repository.insertAndGetId(visit)
//                val task = Task(name = "Some Task", type = TaskType.CHECKUP, taskStatus = TaskStatus.OPEN, communityMemberId = 1, visitId = visitId)
//
//                val record = Record(record = "Helllo", actionItemId = 10L, communityMemberId = 10L, dateRecorded = Date())
//                recordRepository.insert(record)
                _navigateBack.value = true
                _showSnackbarVisitAddedEvent.value = true
            }
            catch (e: SQLiteConstraintException) {
                Log.e("Agenda", "Cannot create a Fake Visit before creating a user")
            }

        }
    }

    fun doneNavigatingBack() {
        _navigateBack.value = false
    }

    fun doneShowingSnackbarVisitAddedEvent() {
        _showSnackbarVisitAddedEvent.value = false
    }
}