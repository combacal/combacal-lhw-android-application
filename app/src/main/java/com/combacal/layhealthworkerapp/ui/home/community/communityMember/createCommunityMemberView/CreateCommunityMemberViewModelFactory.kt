package com.combacal.layhealthworkerapp.ui.home.community.communityMember.createCommunityMemberView

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.combacal.layhealthworkerapp.data.repositories.CommunityMemberRepository

class CreateCommunityMemberViewModelFactory(
    private val repository: CommunityMemberRepository,
    private val application: Application,
    private val householdId: Long
) : ViewModelProvider.Factory {

    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CreateCommunityMemberViewModel::class.java)) {
            return CreateCommunityMemberViewModel(repository, application, householdId) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}