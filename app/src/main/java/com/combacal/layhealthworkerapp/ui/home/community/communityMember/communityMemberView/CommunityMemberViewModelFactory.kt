package com.combacal.layhealthworkerapp.ui.home.community.communityMember.communityMemberView

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.combacal.layhealthworkerapp.data.repositories.CommunityMemberRepository

class CommunityMemberViewModelFactory(
    private val repository: CommunityMemberRepository,
    private val communityMemberId: Long
) : ViewModelProvider.Factory {

    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CommunityMemberViewModel::class.java)) {
            return CommunityMemberViewModel(repository, communityMemberId) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}