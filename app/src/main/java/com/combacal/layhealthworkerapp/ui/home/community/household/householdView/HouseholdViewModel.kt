package com.combacal.layhealthworkerapp.ui.home.community.household.householdView

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.combacal.layhealthworkerapp.data.datasources.daos.HouseholdDao

class HouseholdViewModel(
    database: HouseholdDao,
    application: Application,
    val householdId: Long
) : AndroidViewModel(application){

    val household = database.get(householdId)

    private var _navigateToCommunityMember = MutableLiveData<Long?>()
    val navigateToCommunityMember: LiveData<Long?>
        get() = _navigateToCommunityMember

    fun onCommunityMemberClicked(communityMemberId: Long) {
        _navigateToCommunityMember.value = communityMemberId
    }

    fun doneNavigatingToCommunityMember() {
        _navigateToCommunityMember.value = null
    }

}