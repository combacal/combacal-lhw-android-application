package com.combacal.layhealthworkerapp.ui.home.community.household.createHouseholdView

import androidx.lifecycle.*
import com.combacal.layhealthworkerapp.data.models.Household
import com.combacal.layhealthworkerapp.data.repositories.HouseholdRepository
import kotlinx.coroutines.launch
import kotlin.random.Random

class CreateHouseholdViewModel(
    val repository: HouseholdRepository,
) : ViewModel() {

    private val  _isFormComplete = MutableLiveData<Boolean>()
    val isFormComplete: LiveData<Boolean>
        get() = _isFormComplete

    private lateinit var householdName : String
    private lateinit var firstNameHeadOfHousehold : String
    private lateinit var phoneNumberHeadOfHousehold : String
    private lateinit var householdDescription : String

    private val _navigateToCommunityView = MutableLiveData<Boolean>()
    val navigateToCommunityView:  LiveData<Boolean>
        get() = _navigateToCommunityView

    fun doneNavigatingToCommunityView() {
        _navigateToCommunityView.value = false
    }

    private var _showSnackbarHouseholdAddedEvent = MutableLiveData<Boolean>()
    val showSnackbarHouseholdAddedEvent: LiveData<Boolean>
        get() = _showSnackbarHouseholdAddedEvent

    fun doneShowingSnackbarHouseholdAddedEvent() {
        _showSnackbarHouseholdAddedEvent.value = false
    }


    fun createHouseholdFormDataChanged(householdName: String,
                                       firstNameHeadOfHousehold: String,
                                       phoneNumberHeadOfHousehold: String,
                                       householdDescription: String) {

        val isAnyFieldEmpty = checkIfAnyFieldIsEmpty(
            listOf(
                householdName,
                firstNameHeadOfHousehold,
                phoneNumberHeadOfHousehold,
                householdDescription
            )
        )

        if (isAnyFieldEmpty) { _isFormComplete.value = false; return }

        this.householdName = householdName
        this.firstNameHeadOfHousehold = firstNameHeadOfHousehold
        this.phoneNumberHeadOfHousehold = phoneNumberHeadOfHousehold
        this.householdDescription = householdDescription
        _isFormComplete.value = true

    }

    private fun checkIfAnyFieldIsEmpty(createHouseholdFormData: List<String>): Boolean {

        return createHouseholdFormData.contains(String())
    }

    fun onAddHouseholdClicked() {
        // https://developer.android.com/kotlin/coroutines
        viewModelScope.launch {
            val latitude = -29.4510 + Random.nextDouble(-0.01, 0.01)
            val longitude = 27.7216 + Random.nextDouble(-0.01, 0.01)
            val household = Household(
                name = householdName,
                head = firstNameHeadOfHousehold,
                phone = phoneNumberHeadOfHousehold,
                locationDescription = householdDescription,
                latitude = latitude,
                longitude = longitude)
            repository.insert(household)
        }
        _navigateToCommunityView.value = true
        _showSnackbarHouseholdAddedEvent.value = true
    }
}