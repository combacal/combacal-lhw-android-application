package com.combacal.layhealthworkerapp.ui.home.community.communityListView

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.combacal.layhealthworkerapp.data.models.Household
import com.combacal.layhealthworkerapp.databinding.ListItemHouseholdBinding


class CommunityListViewAdapter(val clickListener: HouseholdListener) : ListAdapter<Household, CommunityListViewAdapter.HouseholdViewHolder>(HouseholdDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HouseholdViewHolder {
        return HouseholdViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: HouseholdViewHolder, position: Int) {
        holder.bind(getItem(position)!!, clickListener) // pass clickListener down to viewHolder
    }


    class HouseholdViewHolder private constructor(val binding: ListItemHouseholdBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(
            item: Household,
            clickListener: HouseholdListener
        ) {
            binding.household = item
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): HouseholdViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemHouseholdBinding.inflate(layoutInflater, parent, false)

                return HouseholdViewHolder(binding)
            }
        }
    }
}

class HouseholdDiffCallback: DiffUtil.ItemCallback<Household>() {
    override fun areItemsTheSame(oldItem: Household, newItem: Household): Boolean {
        return oldItem.householdId == newItem.householdId
    }

    override fun areContentsTheSame(oldItem: Household, newItem: Household): Boolean {
        return oldItem == newItem
    }
}

class HouseholdListener(val clickListener: (householdId: Long) -> Unit) {
    fun onClick(household: Household) = clickListener(household.householdId)
}