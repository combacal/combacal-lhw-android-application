package com.combacal.layhealthworkerapp.ui.home.agenda

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.combacal.layhealthworkerapp.data.models.Visit
import com.combacal.layhealthworkerapp.databinding.ListItemVisitBinding

class VisitAdapter(val clickListener: VisitListener) : ListAdapter<Visit, VisitAdapter.VisitViewHolder>(
    VisitDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VisitViewHolder {
        return VisitViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: VisitViewHolder, position: Int) {
        holder.bind(getItem(position)!!, clickListener) // pass clickListener down to viewHolder
    }


    class VisitViewHolder private constructor(val binding: ListItemVisitBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(
            item: Visit,
            clickListener: VisitListener
        ) {
            binding.visit = item
            binding.clickListener = clickListener
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): VisitViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ListItemVisitBinding.inflate(layoutInflater, parent, false)

                return VisitViewHolder(binding)
            }
        }
    }

}

class VisitDiffCallback: DiffUtil.ItemCallback<Visit>() {
    override fun areItemsTheSame(oldItem: Visit, newItem: Visit): Boolean {
        return oldItem.visitId == newItem.visitId
    }

    override fun areContentsTheSame(oldItem: Visit, newItem: Visit): Boolean {
        return oldItem == newItem
    }
}

class VisitListener(val clickListener: (visitId: Long) -> Unit) {
    fun onClick(visit: Visit) = clickListener(visit.visitId)
}