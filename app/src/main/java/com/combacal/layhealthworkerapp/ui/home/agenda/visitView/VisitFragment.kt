package com.combacal.layhealthworkerapp.ui.home.agenda.visitView

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.combacal.layhealthworkerapp.data.MainDatabase
import com.combacal.layhealthworkerapp.data.repositories.VisitRepository
import com.combacal.layhealthworkerapp.databinding.FragmentVisitBinding

class VisitFragment : Fragment() {

    private var _binding: FragmentVisitBinding? = null
    private val binding get() = _binding!!

    private lateinit var viewModel: VisitViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentVisitBinding.inflate(inflater, container, false)

        setupAndBindViewModelWithRepository()

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupAndBindViewModelWithRepository() {

        val args = VisitFragmentArgs.fromBundle(requireArguments())
        val application = requireNotNull(this.activity).application
        val datasource = MainDatabase.getInstance(application).visitDao
        val repository = VisitRepository(datasource)
        val viewModelFactory = VisitViewModelFactory(repository, args.visitId)

        viewModel =
            ViewModelProvider(
                this, viewModelFactory
            ).get(VisitViewModel::class.java)

        binding.visitViewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner
    }

}