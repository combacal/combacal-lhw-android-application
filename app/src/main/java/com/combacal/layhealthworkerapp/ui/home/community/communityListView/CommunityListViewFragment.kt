package com.combacal.layhealthworkerapp.ui.home.community.communityListView

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import com.combacal.layhealthworkerapp.data.MainDatabase
import com.combacal.layhealthworkerapp.data.repositories.HouseholdRepository
import com.combacal.layhealthworkerapp.databinding.FragmentCommunityListViewBinding
import com.combacal.layhealthworkerapp.ui.home.community.CommunityViewModel
import com.combacal.layhealthworkerapp.ui.home.community.CommunityViewModelFactory

class CommunityListViewFragment : Fragment() {

    private lateinit var communityViewModel: CommunityViewModel

    private var _binding: FragmentCommunityListViewBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentCommunityListViewBinding.inflate(inflater, container, false)

        setupAndBindViewModelWithRepository()

        val adapter = createAndBindListViewAdapterWithClickListener()
        observeHouseholds(adapter)

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun observeHouseholds(adapter: CommunityListViewAdapter) {
        communityViewModel.households.observe(viewLifecycleOwner, {
            it?.let {
                adapter.submitList(it)
            }
        })
    }

    private fun createAndBindListViewAdapterWithClickListener(): CommunityListViewAdapter {
        val adapter = CommunityListViewAdapter(HouseholdListener { householdId ->
            communityViewModel.onHouseholdClicked(householdId)
        })

        _binding!!.recyclerViewCommunityList.adapter = adapter
        return adapter
    }

    private fun setupAndBindViewModelWithRepository() {
        val application = requireNotNull(this.activity).application
        val datasource = MainDatabase.getInstance(application).householdDao
        val repository = HouseholdRepository(datasource)
        val viewModelFactory = CommunityViewModelFactory(repository, application)

        communityViewModel =
            ViewModelProvider(
                requireActivity(), viewModelFactory
            ).get(CommunityViewModel::class.java)

        _binding!!.communityViewModel = communityViewModel
        _binding!!.lifecycleOwner = viewLifecycleOwner
    }
}