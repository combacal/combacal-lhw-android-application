package com.combacal.layhealthworkerapp.ui.home.community.household.householdView

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.combacal.layhealthworkerapp.data.datasources.daos.HouseholdDao

class HouseholdViewModelFactory(
    private val repository: HouseholdDao,
    private val application: Application,
    private val householdId: Long,
) : ViewModelProvider.Factory {

    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(HouseholdViewModel::class.java)) {
            return HouseholdViewModel(repository, application, householdId) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}