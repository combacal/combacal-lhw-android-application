package com.combacal.layhealthworkerapp.ui.home.community.household.householdView

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.combacal.layhealthworkerapp.data.MainDatabase
import com.combacal.layhealthworkerapp.databinding.FragmentHouseholdBinding
import com.combacal.layhealthworkerapp.ui.home.SupportActionBarListener
import com.combacal.layhealthworkerapp.ui.home.community.communityMember.communityMemberView.CommunityMemberFragmentDirections

class HouseholdFragment : Fragment() {

    private lateinit var viewModel: HouseholdViewModel

    private var _binding: FragmentHouseholdBinding? = null
    private val binding get() = _binding!!


    // -- Lifecycle methods --
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentHouseholdBinding.inflate(inflater, container, false)

        setupAndBindViewModelWithDatasource()
        observeHousehold()
        observeNavigateToCommunityMember()

        val adapter = createAndBindCommunityMemberAdapterWithClickListener()
        observeCommunityMembers(adapter)



        return binding.root
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    // -- Setup methods --

    private fun setupAndBindViewModelWithDatasource() {

        val args = HouseholdFragmentArgs.fromBundle(requireArguments())
        val application = requireNotNull(this.activity).application
        val datasource = MainDatabase.getInstance(application).householdDao
        val viewModelFactory = HouseholdViewModelFactory(datasource, application, args.householdId)

        viewModel =
            ViewModelProvider(
                this, viewModelFactory
            ).get(HouseholdViewModel::class.java)

        _binding!!.householdViewModel = viewModel
        _binding!!.lifecycleOwner = viewLifecycleOwner
    }

    // -- RecyclerView --

    private fun observeCommunityMembers(adapter: CommunityMemberAdapter) {
        viewModel.household.observe(viewLifecycleOwner, {
            it?.let {
                adapter.submitList(it.communityMembers)
            }
        })
    }

    private fun createAndBindCommunityMemberAdapterWithClickListener(): CommunityMemberAdapter {
        val adapter = CommunityMemberAdapter(CommunityMemberListener { communityMemberId ->
            viewModel.onCommunityMemberClicked(communityMemberId)
        })

        _binding!!.recyclerViewCommunityMemberList.adapter = adapter
        return adapter
    }


    // -- Navigation Observers --

    private fun observeNavigateToCommunityMember() {
        viewModel.navigateToCommunityMember.observe(viewLifecycleOwner, { communityMemberId ->
            communityMemberId?.let {
                findNavController().navigate(
                    CommunityMemberFragmentDirections.actionGlobalCommunityMemberFragment(communityMemberId)
                )
                viewModel.doneNavigatingToCommunityMember()
            }
        })
    }

    // -- Variable Observers --

    private fun observeHousehold() {
        viewModel.household.observe(viewLifecycleOwner, {
            it?.let {

                val household = viewModel.household.value?.household

                if (household != null) {
                    (requireActivity() as SupportActionBarListener)
                        .setSupportActionBarTitle(
                            "Household: ${household.name}")

                    binding.textViewHeadOfHousehold.text = "Head of Household: ${household.head}"
                    binding.textViewPhone.text = "Phone Number: ${household.phone}"
                    binding.textViewLocation.text = "Location Description: ${household.locationDescription}"
                }


            }
        })
    }

}