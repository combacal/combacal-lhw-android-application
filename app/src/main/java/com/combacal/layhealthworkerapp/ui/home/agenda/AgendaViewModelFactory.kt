package com.combacal.layhealthworkerapp.ui.home.agenda

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.combacal.layhealthworkerapp.data.repositories.VisitRepository

class AgendaViewModelFactory(
    private val repository: VisitRepository,
) : ViewModelProvider.Factory {

    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(AgendaViewModel::class.java)) {
            return AgendaViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}