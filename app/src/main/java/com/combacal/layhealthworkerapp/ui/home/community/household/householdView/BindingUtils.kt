package com.combacal.layhealthworkerapp.ui.home.community.household.householdView

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.combacal.layhealthworkerapp.R
import com.combacal.layhealthworkerapp.data.models.CommunityMember
import com.combacal.layhealthworkerapp.ui.home.community.convertDateOfBirthToAge

@BindingAdapter("communityMemberName")
fun TextView.setCommunityMemberName(item: CommunityMember?){
    item?.let {
        text = item.firstname
    }
}

@BindingAdapter("communityMemberSex")
fun TextView.setCommunityMemberSex(item: CommunityMember?){
    item?.let {
        text = item.sex.toString()
    }
}


@BindingAdapter("communityMemberAge")
fun TextView.setCommunityMemberAge(item: CommunityMember?){
    item?.let {
        text = convertDateOfBirthToAge(item.dateOfBirth)
    }
}


@BindingAdapter("communityMemberImage")
fun ImageView.setCommunityMemberImage(item: CommunityMember?) {
    item?.let {
        setImageResource(R.drawable.ic_baseline_person_24)
    }
}