package com.combacal.layhealthworkerapp.ui.auth.registration

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import com.combacal.layhealthworkerapp.R
import com.combacal.layhealthworkerapp.data.MainDatabase
import com.combacal.layhealthworkerapp.data.models.User
import com.combacal.layhealthworkerapp.data.repositories.UserRepository
import com.combacal.layhealthworkerapp.data.repositories.VisitRepository
import com.combacal.layhealthworkerapp.databinding.FragmentRegistrationBinding
import com.combacal.layhealthworkerapp.ui.auth.login.LoginViewModel
import com.combacal.layhealthworkerapp.ui.auth.login.LoginViewModelFactory
import com.combacal.layhealthworkerapp.ui.home.agenda.AgendaViewModel
import com.combacal.layhealthworkerapp.ui.home.agenda.AgendaViewModelFactory
import com.combacal.layhealthworkerapp.ui.home.community.household.householdView.HouseholdViewModel
import com.combacal.layhealthworkerapp.ui.home.community.household.householdView.HouseholdViewModelFactory
import com.google.android.material.snackbar.Snackbar

class RegistrationFragment : Fragment() {

    companion object {
        fun newInstance() = RegistrationFragment()
    }

    private lateinit var viewModel: RegistrationViewModel
    private var _binding: FragmentRegistrationBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentRegistrationBinding.inflate(inflater, container, false)

        setupAndBindViewModelWithRepository()
        setupTextWatcherForFormFields()
        observeShowSnackbarErrorEvent()

        (activity as AppCompatActivity).supportActionBar?.setDisplayHomeAsUpEnabled(true)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupAndBindViewModelWithRepository() {
        val application = requireNotNull(this.activity).application
        val datasource = MainDatabase.getInstance(application).userDao
        val repository = UserRepository(datasource)
        val viewModelFactory = RegistrationViewModelFactory(repository, application)

        viewModel = ViewModelProvider(this, viewModelFactory).get(RegistrationViewModel::class.java)

        _binding!!.registrationViewModel = viewModel
        _binding!!.lifecycleOwner = viewLifecycleOwner
    }

    private fun showRegistrationError(errorString: String) {
        val appContext = context?.applicationContext ?: return
        Toast.makeText(appContext, errorString, Toast.LENGTH_LONG).show()
    }

    private fun observeShowSnackbarErrorEvent() {
        viewModel.showSnackbarErrorEvent.observe(viewLifecycleOwner, {
            if (it != null) {
                Snackbar.make(
                    requireActivity().findViewById(R.id.snack_bar_host),
                    it,
                    Snackbar.LENGTH_SHORT
                ).show()
                viewModel.doneShowingSnackbarErrorEvent()
            }
        })
    }

    private fun setupTextWatcherForFormFields() {
        val afterTextChangedListener = object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // ignore
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // ignore
            }

            override fun afterTextChanged(s: Editable?) {
                viewModel.registrationFormDataChanged(
                    binding.editTextFirstname.text.toString(),
                    binding.editTextLastname.text.toString(),
                    binding.editTextUsernameRegistration.text.toString(),
                    binding.editTextPhoneNumber.text.toString(),
                    binding.editTextAddress.text.toString(),
                    binding.editTextPasswordRegistration.text.toString(),
                    binding.editTextConfirmPassword.text.toString(),
                )
            }
        }

        binding.editTextFirstname.addTextChangedListener(afterTextChangedListener)
        binding.editTextLastname.addTextChangedListener(afterTextChangedListener)
        binding.editTextUsernameRegistration.addTextChangedListener(afterTextChangedListener)
        binding.editTextPhoneNumber.addTextChangedListener(afterTextChangedListener)
        binding.editTextAddress.addTextChangedListener(afterTextChangedListener)
        binding.editTextPasswordRegistration.addTextChangedListener(afterTextChangedListener)
        binding.editTextConfirmPassword.addTextChangedListener(afterTextChangedListener)
    }
}