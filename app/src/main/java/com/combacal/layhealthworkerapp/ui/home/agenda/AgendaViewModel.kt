package com.combacal.layhealthworkerapp.ui.home.agenda

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.combacal.layhealthworkerapp.data.repositories.VisitRepository

class AgendaViewModel(
    repository: VisitRepository,
) : ViewModel() {

    val visits = repository.getAllVisitsAsLiveData()

    private val _navigateToVisit = MutableLiveData<Long?>()
    val navigateToVisit: MutableLiveData<Long?>
        get() = _navigateToVisit

    fun onVisitClicked(visitId: Long) {
        _navigateToVisit.value = visitId
    }

    fun doneNavigatingToVisit() {
        _navigateToVisit.value = null
    }
}