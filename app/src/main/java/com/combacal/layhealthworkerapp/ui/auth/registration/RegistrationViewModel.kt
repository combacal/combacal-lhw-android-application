package com.combacal.layhealthworkerapp.ui.auth.registration

import android.app.Application
import android.util.Log
import android.util.Patterns
import android.widget.Toast
import androidx.lifecycle.*
import com.combacal.layhealthworkerapp.data.models.Household
import com.combacal.layhealthworkerapp.data.models.User
import com.combacal.layhealthworkerapp.data.repositories.UserRepository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.random.Random

class RegistrationViewModel(
    val repository: UserRepository,
    application: Application,
): AndroidViewModel(application) {

    private val  _isFormComplete = MutableLiveData<Boolean>()
    val isFormComplete: LiveData<Boolean>
        get() = _isFormComplete

    private lateinit var firstname : String
    private lateinit var lastname : String
    private lateinit var username : String
    private lateinit var phoneNumber : String
    private lateinit var address : String
    private lateinit var password: String
    private lateinit var confirmPassword : String


    private val _showSnackbarErrorEvent = MutableLiveData<String?>()
    val showSnackbarErrorEvent: LiveData<String?>
        get() = _showSnackbarErrorEvent

    init {
        _showSnackbarErrorEvent.value = null
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return if (username.contains("@")) {
            Patterns.EMAIL_ADDRESS.matcher(username).matches()
        } else {
            username.isNotBlank()
        }
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.length > 5
    }

    private fun checkIfAnyFieldIsEmpty(registrationFormData: List<String>): Boolean {
        return registrationFormData.contains(String())
    }

    fun registrationFormDataChanged(
        firstname: String,
        lastname: String,
        username: String,
        phoneNumber: String,
        address: String,
        password: String,
        confirmPassword: String
    ) {

        val isAnyFieldEmpty = checkIfAnyFieldIsEmpty(
            listOf(
                firstname,
                lastname,
                username,
                phoneNumber,
                address,
                password,
                confirmPassword
            )
        )

        if (isAnyFieldEmpty) { _isFormComplete.value = false; return }

        this.firstname = firstname
        this.lastname = lastname
        this.username = username
        this.phoneNumber = phoneNumber
        this.address = address
        this.password = password
        this.confirmPassword = confirmPassword

        _isFormComplete.value = true
    }

    fun doneShowingSnackbarErrorEvent() {
        _showSnackbarErrorEvent.value = null
    }

    fun onCreateAccountClicked() {
        if (_isFormComplete.value != true) {
            return
        }
        // Check if the confirmation password matches
        if (confirmPassword != password) {
            Log.i("MYTAG", "The passwords do not match!")
            _showSnackbarErrorEvent.value = "The passwords do not match!"
            return
        }
        // https://developer.android.com/kotlin/coroutines
        viewModelScope.launch {
            // Check if a user with the same username already exits
            val checkUser = repository.getByUserName(username)
            if (checkUser != null) {
                Log.i("MYTAG", "User is already registered!")
                _showSnackbarErrorEvent.value = "There is a registered account with the same username!"
            } else {
                Log.i("MYTAG", "Save new user!")
                val user = User(
                    firstName = firstname,
                    lastName = lastname,
                    userName = username,
                    phoneNumber = phoneNumber,
                    address = address,
                    password = password
                )
                repository.insert(user)
            }
        }
    }
}