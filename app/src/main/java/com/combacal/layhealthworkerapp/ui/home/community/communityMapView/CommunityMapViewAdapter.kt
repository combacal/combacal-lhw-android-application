package com.combacal.layhealthworkerapp.ui.home.community.communityMapView

import android.util.Log
import com.combacal.layhealthworkerapp.data.models.Household
import com.google.gson.JsonPrimitive
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.plugins.annotation.Symbol
import com.mapbox.mapboxsdk.plugins.annotation.SymbolManager
import com.mapbox.mapboxsdk.plugins.annotation.SymbolOptions


class CommunityMapViewAdapter(
    private val symbolManager: SymbolManager
)  {

    var data = listOf<Household>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    private var symbols = ArrayList<Symbol>()


    private fun createSymbolList() {
        val symbolOptionsList = ArrayList<SymbolOptions>()
        for (household in data) {
            symbolOptionsList.add(createHouseholdSymbolOption(household.latitude,
                household.longitude,
                household.householdId))
        }
        symbols = symbolManager.create(symbolOptionsList) as ArrayList<Symbol>
    }

    // At some point we don't want to replace all the symbols but only remove and add what has changed
    // We need something that finds the difference between the old and the new symbol list
    private fun updateSymbolList() {
        symbolManager.delete(symbols)

        val options = ArrayList<SymbolOptions>()
        for (household in data) {
            options.add(createHouseholdSymbolOption(household.latitude,
                household.longitude,
                household.householdId))
        }
        symbols = symbolManager.create(options) as ArrayList<Symbol>
    }

    private fun createHouseholdSymbolOption(
        latitude: Double,
        longitude: Double,
        householdID: Long,
    ): SymbolOptions {
        return SymbolOptions()
            .withLatLng(LatLng(latitude, longitude))
            .withIconImage("home")
            .withIconSize(0.8f)
            .withData(JsonPrimitive(householdID))
    }

    private fun notifyDataSetChanged() {
        if (data.isEmpty()) return

        if (symbols.isEmpty()) createSymbolList()
        else updateSymbolList()
    }



}