package com.combacal.layhealthworkerapp.ui.home.community.communityListView

import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.combacal.layhealthworkerapp.R
import com.combacal.layhealthworkerapp.data.models.Household

@BindingAdapter("householdName")
fun TextView.setHouseholdName(item: Household?){
    item?.let {
        text = item.name
    }
}

@BindingAdapter("householdImage")
fun ImageView.setHouseholdImage(item: Household?) {
    item?.let {
        setImageResource(R.drawable.ic_home_black_24dp)
    }
}