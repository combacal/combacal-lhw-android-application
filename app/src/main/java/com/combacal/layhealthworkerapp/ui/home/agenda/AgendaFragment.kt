package com.combacal.layhealthworkerapp.ui.home.agenda

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.combacal.layhealthworkerapp.R
import com.combacal.layhealthworkerapp.data.MainDatabase
import com.combacal.layhealthworkerapp.data.repositories.VisitRepository
import com.combacal.layhealthworkerapp.databinding.FragmentAgendaBinding

class AgendaFragment : Fragment() {

    private lateinit var viewModel: AgendaViewModel
    private var _binding: FragmentAgendaBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAgendaBinding.inflate(inflater, container, false)

        setupAndBindViewModelWithRepository()
        val adapter = createAndBindVisitAdapterWithClickListener()
        observeVisits(adapter)
        observeNavigateToVisit()

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setupAndBindViewModelWithRepository() {
        val application = requireNotNull(this.activity).application
        val datasource = MainDatabase.getInstance(application).visitDao
        val repository = VisitRepository(datasource)
        val viewModelFactory = AgendaViewModelFactory(repository)

        viewModel =
            ViewModelProvider(
                requireActivity(), viewModelFactory
            ).get(AgendaViewModel::class.java)

        _binding!!.agendaViewModel = viewModel

        // Set itself as the lifecycleOwner of the binding
        _binding!!.lifecycleOwner = viewLifecycleOwner
    }


    // -- RecyclerView --
    private fun observeVisits(adapter: VisitAdapter) {
        viewModel.visits.observe(viewLifecycleOwner, {
            it?.let {
                adapter.submitList(it)
            }
        })
    }

    private fun createAndBindVisitAdapterWithClickListener(): VisitAdapter {
        val adapter = VisitAdapter(VisitListener { visitId ->
            viewModel.onVisitClicked(visitId)
        })

        _binding!!.recyclerViewVisitsList.adapter = adapter
        return adapter
    }

    // -- Navigation Observers --
    private fun observeNavigateToVisit() {
        viewModel.navigateToVisit.observe(viewLifecycleOwner, { visitId ->
            visitId?.let {
                findNavController().navigate(
                    AgendaFragmentDirections.actionGlobalVisitFragment(visitId)
                )
                viewModel.doneNavigatingToVisit()
            }
        })
    }

}