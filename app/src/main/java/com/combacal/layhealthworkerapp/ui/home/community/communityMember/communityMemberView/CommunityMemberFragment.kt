package com.combacal.layhealthworkerapp.ui.home.community.communityMember.communityMemberView

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.combacal.layhealthworkerapp.R
import com.combacal.layhealthworkerapp.data.MainDatabase
import com.combacal.layhealthworkerapp.data.repositories.CommunityMemberRepository
import com.combacal.layhealthworkerapp.databinding.FragmentCommunityMemberBinding
import com.combacal.layhealthworkerapp.ui.home.SupportActionBarListener

class CommunityMemberFragment : Fragment() {


    private lateinit var viewModel: CommunityMemberViewModel

    private var _binding: FragmentCommunityMemberBinding? = null
    private val binding
        get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {

        _binding = FragmentCommunityMemberBinding.inflate(inflater, container, false)

        setupAndBindViewModelWithDatasource()
        observeCommunityMember()


        return inflater.inflate(R.layout.fragment_community_member, container, false)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    // -- Setup --

    private fun setupAndBindViewModelWithDatasource() {
        val args = CommunityMemberFragmentArgs.fromBundle(requireArguments())
        val application = requireNotNull(this.activity).application
        val datasource = MainDatabase.getInstance(application).communityMemberDao
        val repository = CommunityMemberRepository(datasource)
        val viewModelFactory = CommunityMemberViewModelFactory(repository, args.communityMemberId)

        viewModel =
            ViewModelProvider(
                this, viewModelFactory
            ).get(CommunityMemberViewModel::class.java)

        _binding!!.communityMemberViewModel = viewModel
        _binding!!.lifecycleOwner = viewLifecycleOwner
    }


    // -- Variable Observers --
    private fun observeCommunityMember() {
        viewModel.communityMember.observe(viewLifecycleOwner, {
            it?.let {
                (requireActivity() as SupportActionBarListener)
                    .setSupportActionBarTitle(
                        viewModel.communityMember.value?.firstname.toString())
            }
        })
    }


}