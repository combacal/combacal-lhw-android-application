package com.combacal.layhealthworkerapp.medical

import java.util.*

interface IFlowAssistant {
    // Returns the id of the first action item
    fun getFirstActionItem(taskId: Long): Long

    fun isNextActionItem(taskId: Long): Boolean

    // Returns the id of next action item to ask the LHW
    fun getNextActionItem(recordId: Long, taskId: Long): Long

    // Returns a result A result is the ID of a new Algorithm and when it has to be performed
    fun getNewAlgorithmIdAndDueDate(taskId: Long): Array<Pair<Long, Date>>
}
