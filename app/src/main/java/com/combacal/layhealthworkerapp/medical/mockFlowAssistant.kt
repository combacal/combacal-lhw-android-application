package com.combacal.layhealthworkerapp.medical

import java.util.*

class mockFlowAssistant : IFlowAssistant {
    var counter = 1L

    override fun getFirstActionItem(taskId: Long): Long {
        return counter
    }

    override fun isNextActionItem(taskId: Long): Boolean {
        if (counter > 3) {
            return false
        }
        return true
    }

    override fun getNextActionItem(recordId: Long, taskId: Long): Long {
        counter ++
        return counter
    }

    override fun getNewAlgorithmIdAndDueDate(taskId: Long): Array<Pair<Long, Date>> {
        return arrayOf(Pair(1L, Date()))
    }
}