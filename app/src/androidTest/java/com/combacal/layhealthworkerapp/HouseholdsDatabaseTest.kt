package com.combacal.layhealthworkerapp

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.combacal.layhealthworkerapp.data.models.Household
import com.combacal.layhealthworkerapp.data.MainDatabase
import com.combacal.layhealthworkerapp.data.datasources.daos.HouseholdDao
import junit.framework.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import timber.log.Timber
import java.io.IOException


@RunWith(AndroidJUnit4::class)
class HouseholdsDatabaseTest {

    private lateinit var householdsDao: HouseholdDao
    private lateinit var db: MainDatabase

    @Before
    fun createDb() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        // Using an in-memory database because the information stored here disappears when the
        // process is killed.
        db = Room.inMemoryDatabaseBuilder(context, MainDatabase::class.java)
            // Allowing main thread queries, just for testing.
            .allowMainThreadQueries()
            .build()
        householdsDao = db.householdDao
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    // first, very basic (unnecessary) test for database
    @Test
    @Throws(Exception::class)
    fun insertAndGetHousehold() {
        Timber.i("insertAndGetHousehold")

        val household = Household(
            name = "Test Household",
            latitude = 24.1,
            longitude = 25.0,
            head = "Head of test Houshold",
            phone = "1234",
            locationDescription = "an der Limmat"
        )

        householdsDao.insert(household)

        val queriedHousehold = householdsDao.getByName(household.name)
        assertEquals(
            queriedHousehold.name,
            "Test Household"
        )
    }
}
